import changePassword from './change-password'
import login from './login'
import initial from './initial'
import register from './register'
import reset from './reset'
import store from './store'


export default {
  changePassword,
  login,
  initial,
  register,
  reset,
  store,
}
