export default {
  root: {
    display: 'block',
    minHeight: 'calc(100vh - 65px - 40px)',
    padding: 20,
  },

  form: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
  },

  talk: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    previous: {
      marginRight: 20,
    },
  },

  description: {
    marginBottom: 20,
  },

  title: {
    margin: 0,
    marginBottom: 20,
  },

  content: {
    padding: 20,
  },

  subtitle: {
    marginBottom: 20,
  },
}
